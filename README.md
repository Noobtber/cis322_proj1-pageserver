# README #

Created by Justin Becker (jbecker4@uoregon.edu)

This is a basic web server. It can return any files hosted in it's pages folder, or return a photo of a cat, if not specified.

It only responds to GET requests.

* Test deployment in other environments. Deployment should work "out of the box" with this command sequence: 

  ```
  git clone <yourGitRepository> <targetDirectory>
  ```
  
  ```
  cd <targetDirectory>
  ```
  
  ```
  make run or make start
  ```
  
  *test it with a browser now, while your server is running in a background process*

  ```
  make stop
  ```
  
* Alternatively, use the script under "tests" folder to test the expected outcomes in an automated fashion. It is accompanied by README file and comments (inside tests.sh) explaining how to test your code.
* Check and revise your `credentials/credentials.ini` file. My grading robots will read this. Be precise. My grading robots are not very good at guessing what you meant to write.
* Turn in the `credentials.ini` file in Canvas. My grading robots will use this file to access your github repository.